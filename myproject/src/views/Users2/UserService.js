const userService = {
  userList: [
    { id: 1, name: 'Siravich', gender: 'M' },
    { id: 2, name: 'Reancharean', gender: 'M' }
  ],
  lastId: 1,
  addUser: function (user) {
    user.id = this.lastId++
    this.userList.push(user)
  },
  updateUser: function (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
  },
  deleteUser: function (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1)
  },
  getUsers () {
    return [...this.userList]
  }
}

export default userService
